var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllersWithViews();
var app = builder.Build();

//app.MapDefaultControllerRoute();
/*app.MapControllerRoute(
    name: "default",
    pattern: "Accueil",
    defaults: new {controller = "Home", action = "Index"}
    );
*/
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}/{nom?}"
    );
app.UseStaticFiles();

app.UseRouting();

app.Run();
