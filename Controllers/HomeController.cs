﻿using Microsoft.AspNetCore.Mvc;

namespace CreerNotrePropreMVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public string IndexString(int id)
        {
            var nom = this.Request.Query["nom"];
            return "Ceci est la méthode d'action string avec paramètres Id:"+ id+ "et le nom :" +nom;
        }
    }
}
